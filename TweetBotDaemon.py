#!/usr/bin/python2.7
#-*- coding: utf-8 -*
'''
Created on 2014. 5. 29.

@author: cinos81
트윗봇 데몬 
'''
import Services
import os
import time
import sys
from Services.EmailService import EmailService

class Jap2KorTranslationNoteDaemon(Services.LoggableDaemon):
    _emailService = {}
    '''
    usage: python TweetBotDaemon.py start|stop|restart|test 3600
    '''
    def __init__(self, pidfile, argv):
        try:
            super(Jap2KorTranslationNoteDaemon, self).__init__(pidfile)
            alramSubject = u'예외가 발생했습니다!'.encode('utf-8')
            _emailService = EmailService(subject=alramSubject)

            #서비스 초기화
            runningMode = 'start'
            if len(argv) >= 2:
                runningMode = argv[1]
                self.evernoteDataService = Services.EvernoteDataServiceImpl(runningMode)
                self.twitterDataService = Services.TwitterDataServiceImpl(runningMode)
        except Exception as ex:
            self.logging.critical('번역 노트봇 초기화 에러 : {0}'.format(ex))
            alramText = u'데몬의 상태를 확인하세요!\n{0}'.format(ex).encode('utf-8')
            self._emailService.setText(alramText).send_email()
            sys.exit(2)
            
    def _postTweet(self, **kwargs):
        '''
        트윗 개시
        '''
        listingOption = {'formatter': Services.EvernoteDataServiceImpl.tweetFormatter}
        for tweetRecord in self.evernoteDataService.getDataList(**listingOption):
            time.sleep(int(kwargs.get('interval')))
            self.twitterDataService.setData(tweetRecord)
            self.logging.info('트윗 전송')
        self.logging.info('트윗 전송 1회전 완료')
        
    def run(self, **kwargs):
        #서비스 초기화
        self.logging.info('다음 모드로 서비스 개시 : {0} {1}'.format(kwargs.get('mode'), kwargs.get('interval')))
        while True:
            try:
                self._postTweet(**kwargs)
            except Exception as ex:
                self.logging.critical('트윗 포스팅 에러 : {0}'.format(ex))
                alramText = u'데몬의 상태를 확인하세요!\n{0}'.format(ex).encode('utf-8')
                self._emailService.setText(alramText).send_email()
        self.logging.info('서비스 종료')

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        pidName = os.path.dirname(os.path.abspath(__file__)).replace('/', '_')
        tweetDaemon = Jap2KorTranslationNoteDaemon('/tmp/{0}.pid'.format(pidName), sys.argv)
        if 'start' == sys.argv[1] or 'test' == sys.argv[1] or 'restart' == sys.argv[1]:
            #시작시 로그를 삭제
            open('jap2korTranslationNoteBot.log', 'w').close()
        
        if 'start' == sys.argv[1] or 'test' == sys.argv[1]:
            args = dict(mode=sys.argv[1])
            if len(sys.argv) >= 3 and sys.argv[2].isdigit():
                args['interval'] = sys.argv[2]
            tweetDaemon.start(**args)
        elif 'stop' == sys.argv[1]:
            tweetDaemon.stop()
        elif 'restart' == sys.argv[1]:
            tweetDaemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)       
 
        print '일본어 번역노트 트윗봇 데몬 종료됨'
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart|test 3600".format(sys.argv[0])
        os.remove('jap2korTranslationNoteBot.log')
        sys.exit(2)