# coding: utf-8
from ConfigParser import ConfigParser

__author__ = 'cinos81'
from Services.TumblerDataService import TumblerDataServiceImpl
from evernote.api.client import EvernoteClient
from evernote.edam.error.ttypes import EDAMUserException, EDAMNotFoundException, EDAMSystemException
from evernote.edam.type.ttypes import Note, SharedNotebook, SharedNotebookPrivilegeLevel

def getUserShardId(authToken, userStore):
    """
    Get the User from userStore and return the user's shard ID
    """
    try:
        user = userStore.getUser(authToken)
    except (EDAMUserException, EDAMSystemException), e:
        print "Exception while getting user's shardID:"
        print type(e), e
        return None
    if hasattr(user, 'shardId'):
        return user.shardId
    return None

def shareSingleNote(authToken, noteStore, userStore, noteGuid, shardId=None):
    """
    Share a single note and return the public URL for the note
    """
    if not shardId:
        shardId = getUserShardId(authToken, userStore)
        if not shardId:
            raise SystemExit

    try:
        shareKey = noteStore.shareNote(authToken, noteGuid)
    except (EDAMNotFoundException, EDAMSystemException, EDAMUserException), e:
        print "Error sharing note:"
        print type(e), e
        raise SystemExit
    finally:
        EN_URL = 'https://sandbox.evernote.com'
        return "%s/shard/%s/sh/%s/%s" % (EN_URL, shardId, noteGuid, shareKey)

def makeNote(authToken, noteStore, noteTitle, noteBody , notebookGuid, tags=None, parentNotebook=None):
    nBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    nBody += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">"
    nBody += "<en-note>%s</en-note>" % noteBody

    # # Create note object
    ourNote = Note()
    ourNote.notebookGuid = notebookGuid
    ourNote.email = "cinos81@gmail.com"
    ourNote.username = "SEO JAE WON"
    ourNote.title = noteTitle
    if tags:
        ourNote.tagNames = tags
    ourNote.content = nBody

    ## parentNotebook is optional; if omitted, default notebook is used
    if parentNotebook and hasattr(parentNotebook, 'guid'):
        ourNote.notebookGuid = parentNotebook.guid

    ## Attempt to create note in Evernote account
    try:
        note = noteStore.createNote(authToken, ourNote)
    except EDAMUserException, edue:
        ## Something was wrong with the note data
        ## See EDAMErrorCode enumeration for error code explanation
        ## http://dev.evernote.com/documentation/reference/Errors.html#Enum_EDAMErrorCode
        print "EDAMUserException:", edue
        return None
    except EDAMNotFoundException, ednfe:
        ## Parent Notebook GUID doesn't correspond to an actual notebook
        print "EDAMNotFoundException: Invalid parent notebook GUID"
        return None
    ## Return created note object
    return note


if __name__ == '__main__':
    cfg = ConfigParser()
    cfg.read('../../tokens.ini')
    auth_token = cfg.get('evernote', 'oauth_token')
    client = EvernoteClient(token=auth_token, sandbox=False)
    noteStore = client.get_note_store()
    tumblrPosts = TumblerDataServiceImpl().getDataList()

    for post in tumblrPosts:
        body = ''
        for line in post['body'].split('\n'):
            # 모두 ascii로 인코딩 한 다음 다음 전송
            body += ('<div>' + line.encode('utf-8') + '</div>')

        tags = []
        for tag in post['tags']:
            tag = tag.encode('utf-8')
            tags.append(tag)
        title = post['title'].encode('utf-8')

        note = makeNote(auth_token, noteStore, title, body, cfg.get('evernote', 'noteBookGuid'), tags)
        print '노트 작성'
        # 아래의 코드를 활성화 시키면 Limit Rate가 치솟는 관계로 주석처리함
        # shareUrl = shareSingleNote(auth_token, noteStore, client.get_user_store(), note.guid)

    print '전송 완료'