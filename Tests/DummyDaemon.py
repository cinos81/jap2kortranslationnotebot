#-*- coding: utf-8 -*-
'''
Created on 2014. 6. 3.

@author: cinos81
'''
import sys
import os
import Services
import time
import logging.config

#1분마다 로그를 기록하는 데몬테스트 클래스
class DummyDaemon(Services.Daemon):
    '''
    usage: ./Tests/DummyDaemon.py start|stop|restart
    '''
    def __init__(self, pidfile):
        super(DummyDaemon, self).__init__(pidfile)
        #로거 초기화
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        logConfigurationFile = os.path.join(BASE_DIR, 'logconfig.ini')
        logging.config.fileConfig(logConfigurationFile)
        
    def run(self):
        while True:
            time.sleep(1)
            logging.info('dummyDaemon is working now!')
            
if __name__ == '__main__':
    testDaemon = DummyDaemon('/tmp/daemon-example.pid')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            testDaemon.start()
        elif 'stop' == sys.argv[1]:
            testDaemon.stop()
        elif 'restart' == sys.argv[1]:
            testDaemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        os.remove('jap2korTranslationNoteBot.log')
        sys.exit(2)