# coding: utf-8
from ConfigParser import ConfigParser

from evernote.api.client import EvernoteClient
from evernote.edam.error.ttypes import EDAMUserException, EDAMSystemException
from evernote.edam.notestore.ttypes import NoteFilter, NotesMetadataResultSpec, NotesMetadataList
from evernote.edam.type.ttypes import NoteSortOrder
from bs4 import BeautifulSoup
from Services.TinyUrlService import TinyUrlService

def getUserShardId(authToken, userStore):
    """
    Get the User from userStore and return the user's shard ID
    """
    try:
        user = userStore.getUser(authToken)
    except (EDAMUserException, EDAMSystemException), e:
        print "Exception while getting user's shardID:"
        print type(e), e
        return None
    if hasattr(user, 'shardId'):
        return user.shardId
    return None

if __name__ == '__main__':
    '''
    이 테스트는 검색조건을 따로 설정하지 않습니다.
    '''

    cfg = ConfigParser()
    cfg.read('../../tokens.ini')
    auth_token = cfg.get('evernote', 'oauth_token')
    client = EvernoteClient(token=auth_token, sandbox=False)
    note_store = client.get_note_store()
    noteBookGuid = cfg.get('evernote', 'noteBookGuid')

    #해당하는 노트북 내의 노트수 검색
    count_filter = NoteFilter(notebookGuid=noteBookGuid)
    notebookCounts = note_store.findNoteCounts(auth_token, count_filter, False).notebookCounts[noteBookGuid]

    #해당하는 노트북 내의 공유노트 취득
    max_notes = 100

    result_spec = NotesMetadataResultSpec(
        includeTitle=True,
        includeContentLength=False,
        includeCreated=False,
        includeUpdated=False,
        includeDeleted=False,
        includeUpdateSequenceNum=False,
        includeNotebookGuid=False,
        includeTagGuids=False,
        includeAttributes=False,
        includeLargestResourceMime=False,
        includeLargestResourceSize=False
    )
    updated_filter = NoteFilter(order=NoteSortOrder.CREATED, notebookGuid=noteBookGuid)

    result_lists = []
    for nextOffset in xrange(0, notebookCounts, max_notes):
        result_lists.append(note_store.findNotesMetadata(auth_token, updated_filter, nextOffset, max_notes, result_spec))

    # note is an instance of NoteMetadata
    # result_list is an instance of NotesMetadataList
    shardId = getUserShardId(auth_token, client.get_user_store())
    noteStore = client.get_note_store()
    for result_list in result_lists:
        for note in result_list.notes:
            print '*' * 100

            noteGuid = note.guid
            content = note_store.getNoteContent(auth_token, noteGuid)

            dom = BeautifulSoup(content)
            lines = dom.find('en-note').find_all('div')
            tweetBody = ''
            for line in lines:
                if line.contents:
                    tweetBody += (line.contents[0] + '\n')
            if len(tweetBody) > 140:
                tweetBody = tweetBody[:140] + '...'

            shareKey = noteStore.shareNote(auth_token, noteGuid)
            shareUrl = "%s/shard/%s/sh/%s/%s" % (cfg.get('evernote', 'en_url'), shardId, noteGuid, shareKey)
            args = {'originUrl': shareUrl}
            shortUrl = TinyUrlService().getData(**args)
            tweetRecord = '{0}\n{1}'.format(tweetBody.encode('utf-8'), shortUrl)

            print tweetRecord