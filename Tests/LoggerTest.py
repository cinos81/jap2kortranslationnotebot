#-*- coding: utf-8 -*-
'''
Created on 2014. 6. 2.

@author: cinos81

로거가 정상 작동하는지 여부를 테스트
'''
import unittest
import logging.config
import os
import re

class Test(unittest.TestCase):
    logFileName = 'jap2korTranslationNoteBot.log'
    infoMsg = 'info msg'
    debugMsg = 'debug msg'
    criticalMsg = 'critical msg'
    warningMsg = 'warning msg'
    errorMsg = 'error msg'
    
    
    def _removeLogFile(self):
        if os.path.exists(self.logFileName):
            os.remove(self.logFileName)
            
    def _writeLog(self):
        logging.info(self.infoMsg)
        logging.debug(self.debugMsg)
        logging.critical(self.criticalMsg)
        logging.warning(self.warningMsg)
        logging.error(self.errorMsg)

    def _testLog(self):
        self.assertTrue(os.path.exists(self.logFileName), '로그파일 기록 실패')
        words = re.findall(self.infoMsg, open(self.logFileName).read().lower())
        self.assertEqual(len(words), 1, 'info 메세지 작성에 이상이 있음. {0}'.format(words))
        words = re.findall(self.debugMsg, open(self.logFileName).read().lower())
        self.assertEqual(len(words), 1, 'debug 메세지 작성에 이상이 있음. {0}'.format(words))
        words = re.findall(self.criticalMsg, open(self.logFileName).read().lower())
        self.assertEqual(len(words), 1, 'critical 메세지 작성에 이상이 있음. {0}'.format(words))
        words = re.findall(self.warningMsg, open(self.logFileName).read().lower())
        self.assertEqual(len(words), 1, 'warning 메세지 작성에 이상이 있음. {0}'.format(words))
        words = re.findall(self.errorMsg, open(self.logFileName).read().lower())
        self.assertEqual(len(words), 1, 'error 메세지 작성에 이상이 있음. {0}'.format(words))

    def test_logFileWrite(self):
        self._removeLogFile()
        
        #로거 초기화
        logging.basicConfig(
            filename=self.logFileName,
            level=logging.DEBUG,
            format='%(levelname)s:%(asctime)s:%(message)s'
        )
        
        self._writeLog()
        self._testLog()
        self._removeLogFile()
        
    def test_logFileWriteFromConfigFile(self):
        self._removeLogFile()
        
        #로거 초기화
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        logConfigurationFile = os.path.join(BASE_DIR, 'logconfig.ini')
        logging.config.fileConfig(logConfigurationFile)
        
        self._writeLog()
        self._testLog()
        self._removeLogFile()
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()