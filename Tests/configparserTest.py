#-*- coding: utf-8 -*
'''
Created on 2014. 6. 13.

@author: cinos81
'''
import unittest
from ConfigParser import ConfigParser


class Test(unittest.TestCase):


    def testConfigParser(self):
        cfg = ConfigParser()
        cfg.read('tokens.ini')
        self.assertEqual('xx', cfg.get('tumblr','consumer_key'), 'tumblr의 consumer_key를 찾을 수 없습니다.')
        self.assertEqual('xx', cfg.get('tumblr','consumer_secret'), 'tumblr의 consumer_secret을 찾을 수 없습니다.')
        self.assertEqual('xx', cfg.get('tumblr','oauth_token'), 'tumblr의 oauth_token를 찾을 수 없습니다.')
        self.assertEqual('xx', cfg.get('tumblr','oauth_token_secret'), 'tumblr의 oauth_token_secret를 찾을 수 없습니다.')
        
        self.assertEqual('xx', cfg.get('twitter','consumer_key'), 'twitter의 consumer_key를 찾을 수 없습니다.')
        self.assertEqual('xx', cfg.get('twitter','consumer_secret'), 'twitter의 consumer_secret을 찾을 수 없습니다.')
        self.assertEqual('xx', cfg.get('twitter','oauth_token'), 'twitter의 oauth_token를 찾을 수 없습니다.')
        self.assertEqual('xx', cfg.get('twitter','oauth_token_secret'), 'twitter의 oauth_token_secret를 찾을 수 없습니다.')

        self.assertEqual('xx', cfg.get('evernote','test_noteBookGuid'), 'evernote의 test_noteBookGuid를 찾을 수 없습니다.')
        self.assertEqual('xx', cfg.get('evernote','test_en_url'), 'evernote의 test_en_url를 찾을 수 없습니다.')
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testConfigParserName']
    unittest.main()
    
