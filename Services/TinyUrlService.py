#-*- coding: utf-8 -*-

'''
Created on 2014. 6. 9.

@author: cinos
'''
from __future__ import with_statement
import contextlib
try:
    from urllib.parse import urlencode
except ImportError:
    from urllib import urlencode
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

class TinyUrlService():
    
    def getData(self, **kwargs):
        if not kwargs['originUrl']:
            return []
        request_url = ('http://tinyurl.com/api-create.php?' + urlencode({'url': kwargs['originUrl']}))
        with contextlib.closing(urlopen(request_url)) as response:
            return response.read().decode('utf-8')

if __name__ == '__main__':
    args = {'originUrl':'http://www.naver.com/'}
    shortUrl = TinyUrlService().getData(**args)
    print shortUrl