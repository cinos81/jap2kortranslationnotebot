#-*- coding: utf-8 -*
'''
Created on 2014. 5. 29.

@author: cinos81
텀블러 데이터 서비스
'''
from random import shuffle
import Services
import pytumblr
#텀블러 데이터 서비스 매니저

class TumblerDataServiceImpl(Services.OAuthDataServiceInterface):
    def _getConsumerClient(self):
        #토큰 설정
        tokens = self._getTokens('tumblr')
        if not self._consumerClient:
            self._consumerClient =  pytumblr.TumblrRestClient(
                tokens.get('consumer_key'),
                tokens.get('consumer_secret'),
                tokens.get('oauth_token'),
                tokens.get('oauth_token_secret')
            )
        return self._consumerClient

    @staticmethod
    def tweetFormatter(self, **kwargs):
        if 'posts' in kwargs:
            posts = kwargs['posts']
        else:
            posts = []

        #포스팅을 렌덤하게 정렬
        shuffle(posts)
        for post in posts:
            #unicode를 str로 타입 변경
            tweetBody = post['body']

            if len(tweetBody) > 140:
                encodedBody = tweetBody[:120].encode('utf-8') + '...'
            else:
                encodedBody = tweetBody.encode('utf-8')
            encodedShortUrl = post.get('short_url').encode('utf-8')
            tweetRecord = '{0}\n{1}\n'.format(encodedBody, encodedShortUrl)
            yield tweetRecord
    
    def getDataList(self, **kwargs):
        tumblrClient = self._getConsumerClient()
         
        #포스팅 취득
        blockSize = 20
        postAllCount = tumblrClient.blog_info('translatejk')['blog']['posts']
        for nextOffset in xrange(0, postAllCount +blockSize, blockSize):
            param = {'offset': nextOffset, 'limit': blockSize, 'filter': 'text'}
            posts = tumblrClient.posts('translatejk', **param).get('posts')

            if 'formatter' in kwargs:
                formatter = kwargs['formatter']
                args = {'posts': posts}
                for post in formatter(self, **args):
                    yield post
            else:
                for post in posts:
                    yield post


if __name__ == '__main__':
    listingOption = {'formatter': TumblerDataServiceImpl.tweetFormatter}
    for post in TumblerDataServiceImpl('test').getDataList(**listingOption):
        print post

    for post in TumblerDataServiceImpl('test').getDataList():
        print post