#-*- coding: utf-8 -*
'''
Created on 2014. 5. 29.

@author: cinos81
트위터 데이터 서비스
'''
import tweepy
import Services
from loremipsum import get_sentences

#트위터 데이터 서비스 매니저
class TwitterDataServiceImpl(Services.OAuthDataServiceInterface):
    def _getConsumerClient(self):
        if not self._consumerClient:
            #토큰 설정
            tokens = self._getTokens('twitter')
            # request_token취득을 요청한다.
            auth = tweepy.OAuthHandler(tokens.get('consumer_key'), tokens.get('consumer_secret'))
            # request_token 에 access_token 과 access_token_secret을 사용해서 서명하도록 요청한다.
            auth.set_access_token(tokens.get('oauth_token'), tokens.get('oauth_token_secret'))
            # 활용 가능힌 상태의 API 참조를 가져온다.
            self._consumerClient = tweepy.API(auth)
        return self._consumerClient
    
    def getDataList(self, **kwargs):
        #트위터 API의 기능중 하나인 타임라인 호출기능을 호출한다.
        return self._getConsumerClient().home_timeline()
    
    def setData(self, record):
        api = self._getConsumerClient()
        try:
            api.update_status(record)
        except Exception as ex:
            # 중복 트윗일 경우 삭제후 다시 트윗한다.
            if ex.message[0]['code'] == 187:
                for status in tweepy.Cursor(api.user_timeline).items():
                    if status.text.encode('utf-8') == record.encode('utf-8'):
                        api.destroy_status(status.id)
                        api.update_status(record)
        
    def delDataList(self):
        #모든 트윗을 삭제한다.
        api = self._getConsumerClient()
        for status in tweepy.Cursor(api.user_timeline).items():
            try:
                api.destroy_status(status.id)
                print "Deleted:", status.id
            except:
                print "Failed to delete:", status.id
                
if __name__ == '__main__':
    deleteAllTweet = raw_input('모든 트윗을 먼저 삭제하시겠습니까? y/n :').strip()
    if deleteAllTweet == 'y':
        print '모든 트윗을 삭제합니다.'
        TwitterDataServiceImpl('test').delDataList()
        print '모든 트윗의 삭제를 완료했습니다.'
    
    tweets = TwitterDataServiceImpl('test').getDataList()
    for tweet in tweets:
        print tweet.text
    print u'{0}건의 트윗 레코드를 출력했습니다.'.format(len(tweets))

    print '테스트 트윗을 5번 실시합니다.'
    for ipsum in get_sentences(5):
        TwitterDataServiceImpl('test').setData(ipsum)
    print '테스트 트윗 완료'

    asis = '\xe8\x87\xaa\xe5\x88\x86\xe3\x81\xae\xe3\x81\xaf\xe3\x81\x88\xe3\x82\x92\xe8\xbf\xbd\xef\xbc\x88\xe3\x81\x8a\xef\xbc\x89\xe3\x81\x84\xe3\x81\x8b\xe3\x81\xad\xe3\x81\xa6\xe3\x81\x84\xe3\x82\x8b:\xeb\x82\xb4 \xec\xbd\x94\xea\xb0\x80 \xec\x84\x9d\xec\x9e\x90\xec\x9d\xb4\xeb\x8b\xa4.\nhttp://tmblr.co/ZycEQn1KgozK6\n'
    tobe = '\xe8\xb6\xb3\xe7\xb9\x81\xef\xbc\x88\xe3\x81\x82\xe3\x81\x97\xe3\x81\x97\xe3\x81\x92\xef\xbc\x89\xe3\x81\x8f\xe3\x81\xab\xe9\x80\x9a\xef\xbc\x88\xe3\x81\x8b\xe3\x82\x88\xef\xbc\x89\xe3\x81\x86\n\xeb\xaa\x87 \xeb\xb2\x88\xec\x9d\xb4\xea\xb3\xa0 \xeb\x8b\xa4\xeb\x8b\x8c\xeb\x8b\xa4[x]\n\xeb\xb0\x9c\xed\x92\x88\xec\x9d\x84 \xed\x8c\x90\xeb\x8b\xa4[o]\n\nhttp://tinyurl.com/l7np95g'
    TwitterDataServiceImpl('test').setData(asis)
    TwitterDataServiceImpl('test').setData(tobe)