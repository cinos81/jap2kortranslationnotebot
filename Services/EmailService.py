#-*- coding: utf-8 -*
from ConfigParser import ConfigParser
import os

__author__ = 'cinos81'

import smtplib

class EmailService():
    _tokens = {}
    _subject = None
    _text = None

    def __init__(self, subject='Testing sending using gmail', text='Testing sending mail using gmail servers'):
        if not self._tokens:
            self._tokens = self._getTokens('google')
        self._subject = subject
        self._text = text

    def setText(self, text):
        self._text = text
        return self

    def _getTokenPath(self):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        tokenConfigurationFile = os.path.join(BASE_DIR, 'tokens.ini')
        return tokenConfigurationFile

    def _getTokens(self, providerName):
        _tokens = {}
        cfg = ConfigParser()
        cfg.read(self._getTokenPath())
        if cfg.has_option(providerName, 'id'):
            _tokens['id'] = cfg.get(providerName, 'id')
        if cfg.has_option(providerName, 'pass'):
            _tokens['pass'] = cfg.get(providerName, 'pass')
        if cfg.has_option(providerName, 'emailFrom'):
            _tokens['emailFrom'] = cfg.get(providerName, 'emailFrom')
        return _tokens

    def send_email(self):
        gmail_user = self._tokens['id']
        gmail_pwd = self._tokens['pass']
        FROM = self._tokens['emailFrom']
        receivers = []
        receivers.append(gmail_user)
        TO = receivers #must be a list
        SUBJECT = self._subject
        TEXT = self._text

        # Prepare actual message
        message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
        """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587) #or port 465 doesn't seem to work!
            server.ehlo()
            server.starttls()
            server.login(gmail_user, gmail_pwd)
            server.sendmail(FROM, TO, message)
            #server.quit()
            server.close()
            print 'successfully sent the mail'
        except Exception as ex:
            print "failed to send mail.\n{0}".format(ex)

if __name__ == '__main__':
    subject = u'예외가 발생했습니다!'.encode('utf-8')
    text = u'데몬의 상태를 확인하세요!\n{0}'.format(u'야호').encode('utf-8')
    emailService = EmailService(subject=subject, text=text).setText(u'텍스트 변경 테스트'.encode('utf-8'))
    emailService.send_email()