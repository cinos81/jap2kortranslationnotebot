#-*- coding: utf-8 -*
'''
Created on 2014. 7. 26.

@author: cinos81
에버노트 데이터 서비스
'''
from Services.TinyUrlService import TinyUrlService
from bs4 import BeautifulSoup
from evernote.api.client import EvernoteClient
from random import shuffle
import Services
from evernote.edam.error.ttypes import EDAMUserException, EDAMSystemException
from evernote.edam.notestore.ttypes import NoteFilter, NotesMetadataResultSpec
from evernote.edam.type.ttypes import NoteSortOrder


class EvernoteDataServiceImpl(Services.OAuthDataServiceInterface):
    def _getUserShardId(self, authToken, userStore):
        """
        Get the User from userStore and return the user's shard ID
        """
        try:
            user = userStore.getUser(authToken)
        except (EDAMUserException, EDAMSystemException), e:
            print "Exception while getting user's shardID:"
            print type(e), e
            return None
        if hasattr(user, 'shardId'):
            return user.shardId
        return None

    def _getConsumerClient(self):
        if not self._consumerClient:
            #토큰 설정
            tokens = self._getTokens('evernote')
            connectionSetting = {'token': tokens['oauth_token'], 'sandbox': False}
            self._consumerClient = EvernoteClient(**connectionSetting)
        return self._consumerClient

    @staticmethod
    def tweetFormatter(self, **kwargs):
        if 'posts' in kwargs:
            posts = kwargs['posts']
        else:
            posts = []

        #토큰 설정
        tokens = self._getTokens('evernote')
        token = tokens['oauth_token']

        #클라이언트 취득
        client = self._getConsumerClient()

        #포스팅을 렌덤하게 정렬
        shuffle(posts)
        for note in posts:
            shardId = self._getUserShardId(token, client.get_user_store())
            noteStore = client.get_note_store()
            noteGuid = note.guid
            content = noteStore.getNoteContent(token, noteGuid)

            dom = BeautifulSoup(content)
            tweetBody = dom.get_text(separator='\n', strip=True)

            #관련번역 이하 문자열 삭제
            bodyToken = u'관련번역'
            if bodyToken in tweetBody:
                indexOfAnnotation = tweetBody.index(bodyToken)
                tweetBody = tweetBody[:indexOfAnnotation]

            #글자를 자를때는 그 형태가 unicode여야 정확하고, third party library에 그 글자를 던질때는 str이어야 한다.
            #뭐야 이게 ㅋㅋㅋㅋ
            if len(tweetBody) > 140:
                tweetBody = tweetBody[:140] + '...'
            tweetBody = tweetBody.encode('utf-8')

            shareKey = noteStore.shareNote(token, noteGuid)
            shareUrl = "%s/shard/%s/sh/%s/%s" % (tokens['en_url'], shardId, noteGuid, shareKey)
            args = {'originUrl': shareUrl}
            shortUrl = TinyUrlService().getData(**args)
            tweetRecord = '{0}\n{1}'.format(tweetBody, shortUrl.encode('utf-8'))

            yield tweetRecord
    
    def getDataList(self, **kwargs):

        #토큰 설정
        tokens = self._getTokens('evernote')
        token = tokens['oauth_token']
        client = self._getConsumerClient()
         
        #노트 저장소 취득
        note_store = client.get_note_store()
        noteBookGuid = tokens['noteBookGuid']

        #해당하는 노트북 내의 노트수 검색
        searchWords = u'-tag:doNotTweet'.encode('utf-8')
        count_filter = NoteFilter(notebookGuid=noteBookGuid, words=searchWords)
        notebookCounts = note_store.findNoteCounts(token, count_filter, False).notebookCounts[noteBookGuid]

        #해당하는 노트북 내의 공유노트 취득
        max_notes = 100
        result_spec = NotesMetadataResultSpec(
            includeTitle=True,
            includeContentLength=False,
            includeCreated=False,
            includeUpdated=False,
            includeDeleted=False,
            includeUpdateSequenceNum=False,
            includeNotebookGuid=False,
            includeTagGuids=False,
            includeAttributes=False,
            includeLargestResourceMime=False,
            includeLargestResourceSize=False
        )
        updated_filter = NoteFilter(order=NoteSortOrder.UPDATED, notebookGuid=noteBookGuid, words=searchWords)

        posts = []
        for nextOffset in xrange(0, notebookCounts, max_notes):
            posts.append(note_store.findNotesMetadata(token, updated_filter, nextOffset, max_notes, result_spec).notes)

        if 'formatter' in kwargs:
            formatter = kwargs['formatter']
            for post in posts:
                args = {'posts': post}
                for post in formatter(self, **args):
                    yield post
        else:
            for post in posts:
                yield post

if __name__ == '__main__':
    client = EvernoteDataServiceImpl('start')
    listingOption = {'formatter': EvernoteDataServiceImpl.tweetFormatter}
    for post in client.getDataList(**listingOption):
        print post

    for post in client.getDataList():
        print type(post)