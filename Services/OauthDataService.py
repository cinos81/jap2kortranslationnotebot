#-*- coding: utf-8 -*-

'''
Created on 2014. 6. 9.

@author: cinos
'''
import os.path
from ConfigParser import ConfigParser

class OAuthDataServiceInterface(object):
    _consumerClient = None
    _runningMode = 'start'
    _tokens = {}

    def __init__(self, runningMode='start'):
        self._runningMode = runningMode
    
    def _getTokenPath(self):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        tokenConfigurationFile = os.path.join(BASE_DIR, 'tokens.ini')
        return tokenConfigurationFile
    
    def _getTokens(self, providerName):
        _tokens = {}
        cfg = ConfigParser()
        cfg.read(self._getTokenPath())
        if self._runningMode == 'test':
            if cfg.has_option(providerName, 'test_consumer_key'):
                _tokens['consumer_key'] = cfg.get(providerName, 'test_consumer_key')
            if cfg.has_option(providerName, 'test_consumer_secret'):
                _tokens['consumer_secret'] = cfg.get(providerName, 'test_consumer_secret')
            if cfg.has_option(providerName, 'test_oauth_token'):
                _tokens['oauth_token'] = cfg.get(providerName, 'test_oauth_token')
            if cfg.has_option(providerName, 'test_oauth_token_secret'):
                _tokens['oauth_token_secret'] = cfg.get(providerName, 'test_oauth_token_secret')
            if cfg.has_option(providerName, 'test_noteBookGuid'):
                _tokens['noteBookGuid'] = cfg.get(providerName, 'test_noteBookGuid')
            if cfg.has_option(providerName, 'test_en_url'):
                _tokens['en_url'] = cfg.get(providerName, 'test_en_url')
        else:
            if cfg.has_option(providerName, 'consumer_key'):
                _tokens['consumer_key'] = cfg.get(providerName, 'consumer_key')
            if cfg.has_option(providerName, 'consumer_secret'):
                _tokens['consumer_secret'] = cfg.get(providerName, 'consumer_secret')
            if cfg.has_option(providerName, 'oauth_token'):
                _tokens['oauth_token'] = cfg.get(providerName, 'oauth_token')
            if cfg.has_option(providerName, 'oauth_token_secret'):
                _tokens['oauth_token_secret'] = cfg.get(providerName, 'oauth_token_secret')
            if cfg.has_option(providerName, 'noteBookGuid'):
                _tokens['noteBookGuid'] = cfg.get(providerName, 'noteBookGuid')
            if cfg.has_option(providerName, 'en_url'):
                _tokens['en_url'] = cfg.get(providerName, 'en_url')
        return _tokens
    
    def _getConsumerClient(self):
        pass
    
    def delDataList(self, **kwargs):
        pass
    
    def getDataList(self, **kwargs):
        pass
    
    def setData(self, record):
        pass

if __name__ == '__main__':
    dataInter = OAuthDataServiceInterface()
    print dataInter._getTokenPath()
    print dataInter._getTokens('twitter')